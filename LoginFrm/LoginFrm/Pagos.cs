﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFrm
{
    public partial class Pagos : Form
    {
        public Pagos()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult confirmacion = MessageBox.Show("Desea procesar el pago?", "Atencion!", MessageBoxButtons.YesNo);
            if (confirmacion== DialogResult.Yes)
            {
                MessageBox.Show("Pago realizado");
            }
            else if (confirmacion==DialogResult.No)
            {
                MessageBox.Show("Pago no procesado");
            }
           


        }

        private void Pagos_Load(object sender, EventArgs e)
        {
           
          
        }

        private void radioButtonE_CheckedChanged(object sender, EventArgs e)
        {
            tbxRoute.Visible = false;
            textBoxE.Visible = true;
            textBoxT.Visible = false;
        }

        private void radioButtonC_CheckedChanged(object sender, EventArgs e)
        {
            tbxRoute.Visible = true;
            textBoxE.Visible = false;
            textBoxT.Visible = false;
        }

        private void radioButtonT_CheckedChanged(object sender, EventArgs e)
        {
            tbxRoute.Visible = false;
            textBoxE.Visible = false;
            textBoxT.Visible = true;
        }
    }
}
