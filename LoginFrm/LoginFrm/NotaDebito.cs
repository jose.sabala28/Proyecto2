﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFrm
{
    public partial class NotaDebito : Form
    {
        public NotaDebito()
        {
            InitializeComponent();
        }

        private void NotaDebito_Load(object sender, EventArgs e)
        {
            tbxDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
    }
}
