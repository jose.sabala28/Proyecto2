﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace LoginFrm
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserAdmin usuarios = new UserAdmin();
            usuarios.MdiParent = this;
            usuarios.Show();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clientes customers = new Clientes();
            customers.Show();
            
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Inventario stock = new Inventario();
            //stock.MdiParent = this;
            stock.Show();
        }

        private void facturacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Facturacion invoice = new Facturacion();
            MenuPrincipal menu = new MenuPrincipal();
            menu.Hide();
            invoice.Show();
           
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Users VentanaUsuario = new Users();
            VentanaUsuario.Show();
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reportes sales = new Reportes();
            sales.Show();

        }

        private void inventarioToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //ReporteInventario stock = new ReporteInventario();
            //stock.Show();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Now.ToString();
        }
    }
}
