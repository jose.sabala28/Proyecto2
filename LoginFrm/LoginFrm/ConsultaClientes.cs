﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFrm
{
    public partial class ConsultaClientes : Form
    {
        public ConsultaClientes()
        {
            InitializeComponent();
        }

        private void ConsultaClientes_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'vasant_Clientes.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.vasant_Clientes.clientes);

        }
    }
}
