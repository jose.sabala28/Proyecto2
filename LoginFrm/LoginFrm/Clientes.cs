﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace LoginFrm
{
    public partial class Clientes : Form
    {
        public Clientes()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            RegistrarCliente regcliente = new RegistrarCliente();
            regcliente.Show();

               
        }

        private void Clientes_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'calzados_VasantDataSet1.clientes' table. You can move, or remove it, as needed.
           // this.clientesTableAdapter.Fill(this.calzados_VasantDataSetCLiente.clientes);

        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    this.clientesTableAdapter.FillBy(this.calzados_VasantDataSetCLiente.clientes, ((int)(System.Convert.ChangeType(codigoToolStripTextBox.Text, typeof(int)))));
            //}
            //catch (System.Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show(ex.Message);
            //}

        }

        private void queryClienteToolStripButton_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    this.clientesTableAdapter.QueryCliente(this.calzados_VasantDataSetCLiente.clientes, ((int)(System.Convert.ChangeType(codigoToolStripTextBox.Text, typeof(int)))));
            //}
            //catch (System.Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show(ex.Message);
            //}

        }

        private void queryIdentificacionToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.clientesTableAdapter.QueryIdentificacion(this.calzados_VasantDataSetCLiente.clientes);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void queryIdentificacionToolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            tbxNombre.ReadOnly = false;
            tbxApellido.ReadOnly = false;
            tbxDireccion.ReadOnly = false;
            tbxTelefono.ReadOnly = false;
            tbxCelular.ReadOnly = false;
            tbxEmail.ReadOnly = false;
            tbxLimite.ReadOnly = false;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ConsultaClientes searchCustomers = new ConsultaClientes();
            searchCustomers.Show();
            
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
