﻿namespace LoginFrm
{
    partial class Pagos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rEfectivo = new System.Windows.Forms.RadioButton();
            this.rCheque = new System.Windows.Forms.RadioButton();
            this.rTarjeta = new System.Windows.Forms.RadioButton();
            this.textBoxE = new System.Windows.Forms.TextBox();
            this.tbxRoute = new System.Windows.Forms.TextBox();
            this.textBoxT = new System.Windows.Forms.TextBox();
            this.groupPay = new System.Windows.Forms.GroupBox();
            this.txtCCexp = new System.Windows.Forms.MaskedTextBox();
            this.txtCSV = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxAcct = new System.Windows.Forms.TextBox();
            this.tbxTotal = new System.Windows.Forms.TextBox();
            this.groupCheck = new System.Windows.Forms.GroupBox();
            this.groupCC = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.groupPay.SuspendLayout();
            this.groupCheck.SuspendLayout();
            this.groupCC.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(102, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Metodo de pago";
            // 
            // rEfectivo
            // 
            this.rEfectivo.AutoSize = true;
            this.rEfectivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rEfectivo.Location = new System.Drawing.Point(20, 32);
            this.rEfectivo.Name = "rEfectivo";
            this.rEfectivo.Size = new System.Drawing.Size(74, 20);
            this.rEfectivo.TabIndex = 1;
            this.rEfectivo.Text = "Efectivo";
            this.rEfectivo.UseVisualStyleBackColor = true;
            this.rEfectivo.CheckedChanged += new System.EventHandler(this.radioButtonE_CheckedChanged);
            // 
            // rCheque
            // 
            this.rCheque.AutoSize = true;
            this.rCheque.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rCheque.Location = new System.Drawing.Point(124, 32);
            this.rCheque.Name = "rCheque";
            this.rCheque.Size = new System.Drawing.Size(73, 20);
            this.rCheque.TabIndex = 2;
            this.rCheque.Text = "Cheque";
            this.rCheque.UseVisualStyleBackColor = true;
            this.rCheque.CheckedChanged += new System.EventHandler(this.radioButtonC_CheckedChanged);
            // 
            // rTarjeta
            // 
            this.rTarjeta.AutoSize = true;
            this.rTarjeta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rTarjeta.Location = new System.Drawing.Point(226, 32);
            this.rTarjeta.Name = "rTarjeta";
            this.rTarjeta.Size = new System.Drawing.Size(69, 20);
            this.rTarjeta.TabIndex = 3;
            this.rTarjeta.Text = "Tarjeta";
            this.rTarjeta.UseVisualStyleBackColor = true;
            this.rTarjeta.CheckedChanged += new System.EventHandler(this.radioButtonT_CheckedChanged);
            // 
            // textBoxE
            // 
            this.textBoxE.Location = new System.Drawing.Point(125, 197);
            this.textBoxE.Name = "textBoxE";
            this.textBoxE.Size = new System.Drawing.Size(100, 20);
            this.textBoxE.TabIndex = 5;
            this.textBoxE.Visible = false;
            // 
            // tbxRoute
            // 
            this.tbxRoute.Location = new System.Drawing.Point(74, 27);
            this.tbxRoute.Name = "tbxRoute";
            this.tbxRoute.Size = new System.Drawing.Size(100, 20);
            this.tbxRoute.TabIndex = 6;
            // 
            // textBoxT
            // 
            this.textBoxT.Location = new System.Drawing.Point(104, 35);
            this.textBoxT.Name = "textBoxT";
            this.textBoxT.Size = new System.Drawing.Size(153, 20);
            this.textBoxT.TabIndex = 7;
            this.textBoxT.Visible = false;
            // 
            // groupPay
            // 
            this.groupPay.Controls.Add(this.rEfectivo);
            this.groupPay.Controls.Add(this.rCheque);
            this.groupPay.Controls.Add(this.rTarjeta);
            this.groupPay.Location = new System.Drawing.Point(105, 45);
            this.groupPay.Name = "groupPay";
            this.groupPay.Size = new System.Drawing.Size(420, 100);
            this.groupPay.TabIndex = 8;
            this.groupPay.TabStop = false;
            this.groupPay.Text = "groupBox1";
            // 
            // txtCCexp
            // 
            this.txtCCexp.Location = new System.Drawing.Point(114, 61);
            this.txtCCexp.Mask = "MM/YY";
            this.txtCCexp.Name = "txtCCexp";
            this.txtCCexp.Size = new System.Drawing.Size(100, 20);
            this.txtCCexp.TabIndex = 9;
            this.txtCCexp.Visible = false;
            // 
            // txtCSV
            // 
            this.txtCSV.Location = new System.Drawing.Point(98, 101);
            this.txtCSV.MaxLength = 4;
            this.txtCSV.Name = "txtCSV";
            this.txtCSV.Size = new System.Drawing.Size(44, 20);
            this.txtCSV.TabIndex = 10;
            this.txtCSV.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "No. Ruta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Fecha Expiracion";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Security Code";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "No. Cuenta";
            // 
            // tbxAcct
            // 
            this.tbxAcct.Location = new System.Drawing.Point(74, 74);
            this.tbxAcct.Name = "tbxAcct";
            this.tbxAcct.Size = new System.Drawing.Size(114, 20);
            this.tbxAcct.TabIndex = 16;
            // 
            // tbxTotal
            // 
            this.tbxTotal.Location = new System.Drawing.Point(671, 64);
            this.tbxTotal.Name = "tbxTotal";
            this.tbxTotal.Size = new System.Drawing.Size(105, 20);
            this.tbxTotal.TabIndex = 18;
            // 
            // groupCheck
            // 
            this.groupCheck.Controls.Add(this.label2);
            this.groupCheck.Controls.Add(this.tbxRoute);
            this.groupCheck.Controls.Add(this.label5);
            this.groupCheck.Controls.Add(this.tbxAcct);
            this.groupCheck.Location = new System.Drawing.Point(365, 174);
            this.groupCheck.Name = "groupCheck";
            this.groupCheck.Size = new System.Drawing.Size(200, 100);
            this.groupCheck.TabIndex = 19;
            this.groupCheck.TabStop = false;
            this.groupCheck.Text = "Cheque";
            this.groupCheck.Visible = false;
            // 
            // groupCC
            // 
            this.groupCC.Controls.Add(this.label7);
            this.groupCC.Controls.Add(this.label4);
            this.groupCC.Controls.Add(this.textBoxT);
            this.groupCC.Controls.Add(this.txtCCexp);
            this.groupCC.Controls.Add(this.txtCSV);
            this.groupCC.Controls.Add(this.label3);
            this.groupCC.Location = new System.Drawing.Point(656, 153);
            this.groupCC.Name = "groupCC";
            this.groupCC.Size = new System.Drawing.Size(293, 162);
            this.groupCC.TabIndex = 0;
            this.groupCC.TabStop = false;
            this.groupCC.Text = "Tarjeta";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(676, 104);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 21;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(580, 64);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(49, 17);
            this.radioButton1.TabIndex = 22;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Total";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(580, 107);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(89, 17);
            this.radioButton2.TabIndex = 23;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Otra cantidad";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Console", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(804, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 37);
            this.label6.TabIndex = 24;
            this.label6.Text = "0.00";
            // 
            // button1
            // 
            this.button1.Image = global::LoginFrm.Properties.Resources.Dollar;
            this.button1.Location = new System.Drawing.Point(376, 318);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 45);
            this.button1.TabIndex = 4;
            this.button1.Text = "Procesar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Image = global::LoginFrm.Properties.Resources.Delete_24x24;
            this.button2.Location = new System.Drawing.Point(490, 318);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 45);
            this.button2.TabIndex = 25;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Numero";
            this.label7.Visible = false;
            // 
            // Pagos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 375);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupCC);
            this.Controls.Add(this.groupCheck);
            this.Controls.Add(this.tbxTotal);
            this.Controls.Add(this.groupPay);
            this.Controls.Add(this.textBoxE);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "Pagos";
            this.Text = "Pagos";
            this.Load += new System.EventHandler(this.Pagos_Load);
            this.groupPay.ResumeLayout(false);
            this.groupPay.PerformLayout();
            this.groupCheck.ResumeLayout(false);
            this.groupCheck.PerformLayout();
            this.groupCC.ResumeLayout(false);
            this.groupCC.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rEfectivo;
        private System.Windows.Forms.RadioButton rCheque;
        private System.Windows.Forms.RadioButton rTarjeta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxE;
        private System.Windows.Forms.TextBox tbxRoute;
        private System.Windows.Forms.TextBox textBoxT;
        private System.Windows.Forms.GroupBox groupPay;
        private System.Windows.Forms.MaskedTextBox txtCCexp;
        private System.Windows.Forms.TextBox txtCSV;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxAcct;
        private System.Windows.Forms.TextBox tbxTotal;
        private System.Windows.Forms.GroupBox groupCheck;
        private System.Windows.Forms.GroupBox groupCC;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label7;
    }
}