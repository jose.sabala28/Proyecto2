﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFrm
{
    public partial class Inventario : Form
    {
        public Inventario()
        {
            InitializeComponent();
        }

        private void Inventario_Load(object sender, EventArgs e)
        {
            tbxDate.Text = DateTime.Today.ToShortDateString();
            // TODO: This line of code loads data into the 'calzados_VasantDataSet.inventario' table. You can move, or remove it, as needed.
            this.inventarioTableAdapter.Fill(this.calzados_VasantDataSet.inventario);

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ConsultaCalzados QShoes = new ConsultaCalzados();
            QShoes.Show();
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            //insert into inventario (id_calzado,cantidad,fecha) values (tbxId,tbxCantidad,Sysdate)
            MessageBox.Show("Desea proesar la entrada?");
        }
    }
}
