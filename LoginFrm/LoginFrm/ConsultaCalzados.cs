﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFrm
{
    public partial class ConsultaCalzados : Form
    {
        public ConsultaCalzados()
        {
            InitializeComponent();
        }

        private void ConsultaCalzados_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'consulta_Calzados.calzados' table. You can move, or remove it, as needed.
            this.calzadosTableAdapter.Fill(this.consulta_Calzados.calzados);

        }
    }
}
