﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using LibreriaData;

namespace LoginFrm
{
    public partial class Login1 : Form
    {
        public Login1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                string comandosql = string.Format("select * from usuarios where usuario='{0}' and clave='{1}' ", tbxUsuario.Text.Trim(), tbxClave.Text.Trim());
                DataSet registro = Recursos.Ejecutar(comandosql);

                string user = registro.Tables[0].Rows[0][2].ToString().Trim();
                string pass = registro.Tables[0].Rows[0][3].ToString().Trim();
                //las posiciones 2 y 3 de la tabla usuarios corresponden a usuario y clave

                //validar que los campos esten llenos
               
                //comparamos los datos digitados
                if (user==tbxUsuario.Text.Trim() && pass==tbxClave.Text.Trim())
                {
                    MessageBox.Show("Sesion iniciada");
                    MenuPrincipal menu = new MenuPrincipal();
                    this.Hide();
                    menu.Show();

                }

            }
            catch (Exception error)
            {

                MessageBox.Show("Usuario o Clave incorrecto");
            }
           
        }
    }
}
