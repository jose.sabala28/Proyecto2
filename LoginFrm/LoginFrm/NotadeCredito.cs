﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFrm
{
    public partial class NotadeCredito : Form
    {
        public NotadeCredito()
        {
            InitializeComponent();
        }

        private void NotadeCredito_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'vasant_ConsultaFactura.factura_detalles' table. You can move, or remove it, as needed.
            this.factura_detallesTableAdapter.Fill(this.vasant_ConsultaFactura.factura_detalles);

        }
    }
}
