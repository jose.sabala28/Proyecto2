﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace LoginFrm
{
    public partial class Devoluciones : Form
    {
        public Devoluciones()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Devoluciones_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'calzados_VasantDataSet2.factura_detalles' table. You can move, or remove it, as needed.
            this.factura_detallesTableAdapter.Fill(this.calzados_VasantDataSet2.factura_detalles);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            NotadeCredito ncredito = new NotadeCredito();
            ncredito.Show();
            this.Hide();
        }
    }
}
