﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibreriaData;

namespace LoginFrm
{
    public partial class Users : Form
    {
        public Users()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //este metodo actualiza la clave del usuario 
            //se crea una variable string que almacena el comando sql utilizado para llamar el procedure
             string cambiarClave = string.Format("execute UpdatePassAdmin '{0}','{1}'", tbxClave.Text.Trim() ,tbxUsuario.Text.Trim());

            //se ejecuta el dataset
            DataSet registroUser = Recursos.Ejecutar(cambiarClave);
        }
    }
}
